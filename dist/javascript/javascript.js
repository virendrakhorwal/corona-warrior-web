document.onreadystatechange = function() { 
  if (document.readyState !== "complete") { 
      document.querySelector( 
        "body").style.visibility = "hidden";
      document.querySelector( 
        "body").style.overflow = "hidden"; 
      document.querySelector( 
        "#loader").style.visibility = "visible"; 
  } else { 
      document.querySelector( 
        "#loader").style.display = "none"; 
      document.querySelector( 
        "body").style.visibility = "visible"; 
      document.querySelector( 
        "body").style.overflow = "visible";
  } 
}; 

window.onload = function(){
  var modalone = document.getElementById("doc1");
  var modaltwo = document.getElementById("doc2");
  var modalthree = document.getElementById("doc3");
  var modalfour = document.getElementById("doc4");
  var modalfive = document.getElementById("doc5");
  var modalsix = document.getElementById("doc6");
  var modalseven = document.getElementById("doc7");
  var modaleight = document.getElementById("doc8");
  var modalnine = document.getElementById("doc9");
  var modalten = document.getElementById("doc10");
  var modaleleven = document.getElementById("doc11");
  var modaltwelve = document.getElementById("doc12");
  var modalthirteen = document.getElementById("doc13");
  var modalfourteen = document.getElementById("doc14");
  var modalfifteen = document.getElementById("doc15");
  var modalsixteen = document.getElementById("doc16");
  var modalseventeen = document.getElementById("doc17");
  var modaleighteen = document.getElementById("doc18");
  var modalnineteen = document.getElementById("doc19");
  var modaltwenty = document.getElementById("doc20");
  var modaltweone = document.getElementById("doc21");
  var modaltwetwo = document.getElementById("doc22");
  var modaltwethree = document.getElementById("doc23");
  var modaltwefour = document.getElementById("doc24");
  var modaltwefive = document.getElementById("doc25");
  var modaltwesix = document.getElementById("doc26");
  var modaltwesev = document.getElementById("doc27");
  var modaltweei = document.getElementById("doc28");

   
  // Get the button that opens the modal
  var btnone = document.getElementById("btn1");
  var btntwo = document.getElementById("btn2");
  var btnthree = document.getElementById("btn3");
  var btnfour = document.getElementById("btn4");
  var btnfive = document.getElementById("btn5");
  var btnsix = document.getElementById("btn6");
  var btnseven = document.getElementById("btn7");
  var btneight = document.getElementById("btn8");
  var btnnine = document.getElementById("btn9");
  var btnten = document.getElementById("btn10");
  var btneleven = document.getElementById("btn11");
  var btntwelve = document.getElementById("btn12");
  var btnthirteen = document.getElementById("btn13");
  var btnfourteen = document.getElementById("btn14");
  var btnfifteen = document.getElementById("btn15");
  var btnsixteen = document.getElementById("btn16");
  var btnseventeen = document.getElementById("btn17");
  var btneighteen = document.getElementById("btn18");
  var btnnineteen = document.getElementById("btn19");
  var btntwenty = document.getElementById("btn20");
  var btntweone = document.getElementById("btn21");
  var btntwetwo = document.getElementById("btn22");
  var btntwethree = document.getElementById("btn23");
  var btntwefour = document.getElementById("btn24");
  var btntwefive = document.getElementById("btn25");
  var btntwesix = document.getElementById("btn26");
  var btntwesev = document.getElementById("btn27");
  var btntweei = document.getElementById("btn28");


  // When the user clicks the button, open the modal 
  btnone.onclick = function() {
    modalone.style.display = "grid";
  }
  btntwo.onclick = function() {
    modaltwo.style.display = "grid";
  }
  btnthree.onclick = function() {
    modalthree.style.display = "grid";
  }
  btnfour.onclick = function() {
    modalfour.style.display = "grid";
  }
  btnfive.onclick = function() {
    modalfive.style.display = "grid";
  }
  btnsix.onclick = function() {
    modalsix.style.display = "grid";
  }
  btnseven.onclick = function() {
    modalseven.style.display = "grid";
  }
  btneight.onclick = function() {
    modaleight.style.display = "grid";
  }
  btnnine.onclick = function() {
    modalnine.style.display = "grid";
  }
  btnten.onclick = function() {
    modalten.style.display = "grid";
  }
  btneleven.onclick = function() {
    modaleleven.style.display = "grid";
  }
  btntwelve.onclick = function() {
    modaltwelve.style.display = "grid";
  }
  btnthirteen.onclick = function() {
    modalthirteen.style.display = "grid";
  }
  btnfourteen.onclick = function() {
    modalfourteen.style.display = "grid";
  }
  btnfifteen.onclick = function() {
    modalfifteen.style.display = "grid";
  }
  btnsixteen.onclick = function() {
    modalsixteen.style.display = "grid";
  }
  btnseventeen.onclick = function() {
    modalseventeen.style.display = "grid";
  }
  btneighteen.onclick = function() {
    modaleighteen.style.display = "grid";
  }
  btnnineteen.onclick = function() {
    modalnineteen.style.display = "grid";
  }
  btntwenty.onclick = function() {
    modaltwenty.style.display = "grid";
  }
  btntweone.onclick = function() {
    modaltweone.style.display = "grid";
  }
  btntwetwo.onclick = function() {
    modaltwetwo.style.display = "grid";
  }
  btntwethree.onclick = function() {
    modaltwethree.style.display = "grid";
  }
  btntwefour.onclick = function() {
    modaltwefour.style.display = "grid";
  }
  btntwefive.onclick = function() {
    modaltwefive.style.display = "grid";
  }
  btntwesix.onclick = function() {
    modaltwesix.style.display = "grid";
  }
  btntwesev.onclick = function() {
    modaltwesev.style.display = "grid";
  }
  btntweei.onclick = function() {
    modaltweei.style.display = "grid";
  }
  

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modalone) {
      modalone.style.display = "none";
    }
    if (event.target == modaltwo) {
      modaltwo.style.display = "none";
    }
    if (event.target == modalthree) {
      modalthree.style.display = "none";
    }
    if (event.target == modalfour) {
      modalfour.style.display = "none";
    }
    if (event.target == modalfive) {
      modalfive.style.display = "none";
    }
    if (event.target == modalsix) {
      modalsix.style.display = "none";
    }
    if (event.target == modalseven) {
      modalseven.style.display = "none";
    }
    if (event.target == modaleight) {
      modaleight.style.display = "none";
    }
    if (event.target == modalnine) {
      modalnine.style.display = "none";
    }
    if (event.target == modalten) {
      modalten.style.display = "none";
    }
    if (event.target == modaleleven) {
      modaleleven.style.display = "none";
    }
    if (event.target == modaltwelve) {
      modaltwelve.style.display = "none";
    }
    if (event.target == modalthirteen) {
      modalthirteen.style.display = "none";
    }
    if (event.target == modalfourteen) {
      modalfourteen.style.display = "none";
    }
    if (event.target == modalfifteen) {
      modalfifteen.style.display = "none";
    }
    if (event.target == modalsixteen) {
      modalsixteen.style.display = "none";
    }
    if (event.target == modalseventeen) {
      modalseventeen.style.display = "none";
    }
    if (event.target == modaleighteen) {
      modaleighteen.style.display = "none";
    }
    if (event.target == modalnineteen) {
      modalnineteen.style.display = "none";
    }
    if (event.target == modaltwenty) {
      modaltwenty.style.display = "none";
    }
    if (event.target == modaltweone) {
      modaltweone.style.display = "none";
    }
    if (event.target == modaltwetwo) {
      modaltwetwo.style.display = "none";
    }
    if (event.target == modaltwethree) {
      modaltwethree.style.display = "none";
    }
    if (event.target == modaltwefour) {
      modaltwefour.style.display = "none";
    }
    if (event.target == modaltwefive) {
      modaltwefive.style.display = "none";
    }
    if (event.target == modaltwesix) {
      modaltwesix.style.display = "none";
    }
    if (event.target == modaltwesev) {
      modaltwesev.style.display = "none";
    }
    if (event.target == modaltweei) {
      modaltweei.style.display = "none";
    }

  }
}


